<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['author_id', 'category_id', 'title', 'img', 'body'];

    public function category()
    {
        return $this->hasOne('App\Category', 'id', 'category_id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'author_id');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }




}
