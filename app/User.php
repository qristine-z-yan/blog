<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use App\Notifications\SendEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    public function posts()
    {
        return $this->hasMany('App\Post', 'author_id', 'id');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment','user_id','id');
    }
}
