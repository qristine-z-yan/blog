<?php

namespace App\Http;

use App\Category;

class helper
{
    public function selectCategories(){
        $categories = Category::all();
        $categories_data = [];
        foreach($categories as $key => $category){
            $categories_data[$category['id']] = $category['name'];
        }
        return $categories_data;
    }
}