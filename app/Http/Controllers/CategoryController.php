<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\helper;
use App\Http\Requests\StoreCategoryRequest;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class CategoryController extends Controller
{
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $helper = new helper;
        $categories_data = $helper->selectCategories();
        return response()->json($categories_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCategoryRequest $request)
    {
        $data = $request->all();
        if ($request->file('category-img')) {
            $img_name = $request->file('category-img')->hashName();
            $upload = $request->file('category-img')->move('img/categories', $img_name);
            if ($upload) {
                $category_data = [
                    'name' => $data['category-name'],
                    'img' => $img_name
                ];
                $category = Category::create($category_data);
                if ($category) {
//                        $request->session()->flash('status', 'Task was successful!');
//                        $request->session()->flash('message', 'Success!');
                    session('type', 'success');
                    return redirect()->back();
                }
            }
        } else {
            return back()->withInput();
        }
    }


    /**
     * Show the posts where category_id is parameter
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::find($id)->load('posts.comments', 'posts.user', 'posts.category');
        if ($category) {
            return response()->json($category);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $del = File::delete(public_path('/img/categories/' . $category->img));
        if ($del) {
            $delete = $category->delete();
            if ($delete) {
                return response(['msg' => 'Category deleted'], 200);
            }
        }
    }
}
