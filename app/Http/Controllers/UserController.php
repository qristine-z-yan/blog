<?php

namespace App\Http\Controllers;


use App\Post;
use App\User;
use Illuminate\Http\File;
use App\Http\Requests\UpdatePostRequest;
use App\Http\Requests\ChangePasswordRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($token, $email)
    {


//        $this->email = $email;
//        $this->token = $token;
//        $user = new User;
//        dd($user);
//        if($user->email == $email && $user->token == $token){
//
//        };
        return view('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
//        if ($id == Auth::user()->id) {
//            return redirect()->route('post.index');
//        } else {
            $user = User::find($id)->load('posts.category', 'posts.comments', 'posts.user');
//            $posts = Post::where('author_id', $id)->get()->load('category', 'user');
            return response()->json($user);
//        }

    }

    public function edit()
    {
        return view('user.update');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePostRequest $request, $id)
    {
        $user_old_data = User::find($id);
        $user_data = $request->all();
        if ($request->hasFile('avatar')) {
            if ($user_old_data->avatar != 'default-avatar.png') {
                File::delete(public_path('img/users/' . $user_old_data->avatar));
            }
            $name = $request->file('avatar')->hashName();
            $move = $request->file('avatar')->move('img/users', $name);
            if ($move) {
                $avatar = $name;
            }
        } else {
            $avatar = $user_old_data->avatar;
        }
        $data = [
            'name' => $user_data['name'],
            'surname' => $user_data['surname'],
            'email' => $user_data['email'],
            'phone' => $user_data['phone'],
            'avatar' => $avatar
        ];
        $update = User::find($id)->update($data);

        if ($update) {
            return redirect()->back()->with('status', 'user-settings');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Change password logged in user
     *
     * @param  input
     * @return \Illuminate\Http\Response
     */

    public function changePassword(ChangePasswordRequest $request)
    {
        $user = Auth::user();
        $old = $request['current-password'];
        $new = $request['new-password'];
        if (!(Hash::check($old, $user['password']))) {
            // The passwords matches
            return redirect()->back()->with("status", "password-matches");
        };
        if (strcmp($old, $new) == 0) {
            //Current password and new password are same
            return redirect()->back()->with("status", "password-error");
        };
        //Change Password
        $user['password'] = bcrypt($old);
        $user->save();
        return redirect()->back()->with("status", "password-changed");
    }
}




