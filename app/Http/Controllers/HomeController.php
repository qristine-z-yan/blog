<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::pluck('name', 'id');
        $posts = Post::with('category', 'user')->get();
        return  view('home', compact('categories', 'posts'));

    }
    public function getPosts()
    {
        $posts = Post::orderBy('id', 'desc')->get()->load('category', 'user', 'comments');
        foreach($posts as $index => $value) {
            if($index % 3 == 0) {
                $arr1[] = $value;
            } elseif($index % 3 == 1) {
                $arr2[] = $value;
            } elseif( $index % 3 == 2) {
                $arr3[] = $value;
            }
        }

        if(isset($arr3)){
            $arr = [ $arr1, $arr2, $arr3];
        } else {
            if (isset($arr2)) {
                $arr = [$arr1, $arr2];
            } else {
                $arr = [$arr1];
            }
        }
        return  response()->json($arr);

    }
}
