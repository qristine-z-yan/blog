<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Support\Facades\Auth;
use App\Category;
use App\Comment;
use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }

    public function index()
    {
        if(Auth::check()) {
            $users = User::all();
            $posts = Post::all();
            $categories = Category::all();
            $comments = Comment::all()->load('user');
            return view('admin.index', compact('users', 'posts', 'categories', 'comments'));
        } else {
            return redirect()->route('home.index');
        }
    }

    public function users()
    {
        $users = User::all()->load('posts');
        return view('admin.users', compact('users'));
    }

    public function updateAdmin(Request $request)
    {
        $user = User::find($request->id);
        if ($user) {
            $user->is_admin = $request->value;
            $update = $user->save();
            if ($update) {
                return response([
                    'msg' => 'Admin changed successfully!'],
                    200);
            } else {
                return response([
                    'msg' => 'Not modified'],
                    304);
            }
        } else {
            return response([
                'msg' => 'Not authorised'],
                401);
        }
    }

    public function posts()
    {
        $posts = Post::all()->load('category', 'user', 'comments');
        return view('admin.posts', compact('posts'));
    }

    public function categories()
    {
        $categories = Category::all()->load('posts');
        return view('admin.categories', compact('categories'));
    }

    public function comments()
    {
        $comments = Comment::all()->load('user', 'children');
        return view('admin.comments', compact('comments'));
    }

}
