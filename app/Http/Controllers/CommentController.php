<?php

namespace App\Http\Controllers;

use App\Events\NewComment;
use App\Http\Requests\UpdateCommentRequest;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Requests\StoreCommentsRequest;
use Illuminate\Support\Facades\Auth;
use App\Comment;
use Illuminate\Support\Facades\Input;


class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCommentsRequest $request)
    {
        if(Auth::check()){
            $query = $request->all();
            $user = Auth::user();
            $data = [
                'user_id' => $user['id'],
                'post_id' => $query['post_id'],
                'parent_id' => $query['parent_id'],
                'comment' => $query['comment'],
            ];
            $add = Comment::create($data);
            $newComment = $add->load('user');

            //Announce that a comment has been added
            broadcast(new NewComment($newComment));

            if($newComment) {
                $post = Post::find($query['post_id'])->load(['comments' => function($q){
                    $q->where('parent_id', null);
                }]);
                $dataNew = array();
                foreach ($post->comments as $comment) {
                    $dataNew[] = $this->get_children($comment);
                }
                if ($dataNew) {
                    return response()->json($dataNew);
                }
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCommentRequest $request, $id)
    {
        $comment = $request['edit-comment'];
        $comment_data = Comment::find($id);
        if ($comment !== $comment_data->comment) {
            $data = [
                'comment' => $comment,
            ];
            $update = Comment::find($id)->update($data);
            if($update){
                return back()->with('status', 'comment-updated');
            }
        } else {
            return back()->with('status', 'comment-error');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $request = request();
        $comment = Comment::find($id);
        $delete = $comment->delete();
        if ($delete) {
            if($request->ajax()){
                return response([
                    'msg' => 'Comment deleted'],
                    200);
            } else {
                return back()->with('status', 'comment-deleted');
            }
        }

    }


    public function get_children($comment) {
        $comment->load('user', 'children.user', 'parent.user');

        if(isset($comment->children)) {
            foreach ($comment->children  as $child) {
                $this->get_children($child);
            }
        }
        return $comment;
    }

    public function showComments(Post $post){
        $post->load(['comments' => function($q){
            $q->where('parent_id', null);
        }]);
        $dataNew = array();
        foreach ($post->comments as $comment) {
            $dataNew[] = $this->get_children($comment);
        }
        if ($dataNew) {
            return response()->json($dataNew);
        }
    }
}