<?php

namespace App\Http\Controllers;

use App\Http\helper;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use App\Http\Requests\UpdatePostRequest;
use App\Http\Requests\StorePostRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use App\Post;
use App\Category;
use App\Comment;
use App\User;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{

    /**
     * Get author posts
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $posts = Post::where('author_id', $user->id)->get();
        $posts->load('category', 'comments', 'user');
        return response()->json($posts);
    }

    /**
     * Go to create post page -- Add Post
     *
     * @return \Illuminate\Http\Response

     **/

    public function create()
    {
      //
    }

    /**
     * Add new post
     *
     * @param StorePostRequest|Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(StorePostRequest $request)
    {
        $data = $request->all();
        if($request->file('img')){
            $img_name = $request->file('img')->hashName();
            $upload = $request->file('img')->move('img/posts', $img_name);
            if($upload){
                $create_data = [
                    'author_id' => Auth::user()->id,
                    'category_id' => $data['category'],
                    'title' => $data['title'],
                    'body' => $data['body'],
                    'img' => $img_name
                ];

                $post = Post::create($create_data);
                if($post){
                    return response()->json('status', 200);
                }
            }
        }
    }

    /**
     * Show the post
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $post = Post::find($id)->load('category', 'user');
        return response()->json($post);
    }

    /**
     * Show the form for editing post.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $helper = new helper;
        $categories_data = $helper->selectCategories();

        $post = Post::find($id);
        return view('post.edit',compact('post', 'categories_data'));
    }

    /**
     * Update the post in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the post from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        $request = request();
        $post = Post::find($id);
        $del = File::delete(public_path('img/posts/' . $post->img));
        if ($del) {
            $delete = $post->delete();
            if ($delete) {
                return response(['msg' => 'Post deleted'], 200);
            }
        }
    }
    public function updatePost(Request $request)
    {
        $data = $request->all();
        $post_data = Post::find($data['id']);
        if($post_data){
            $post_img = $post_data['img'];
            $file = Storage::disk('public')->exists($post_img);
            if ($request->hasFile('img')){

                if($file){
                    File::delete(public_path('img/posts/' . $post_img));
                }
                $name = $request->file('img')->hashName();
                $move = $request->file('img')->move('img/posts',$name);
                if($move){
                    $post_img = $name;
                } else {
                    return response('error', 301);
                }
            } else {
                $post_img = $post_data['img'];
            }
            $edit_data = [
                'author_id' => Auth::user()->id,
                'category_id' => $data['category'],
                'title' => $data['title'],
                'body' => $data['body'],
                'img' => $post_img,
            ];
            $update = Post::find($data['id'])->update($edit_data);
            if ($update) {
                return response('post updated', 200);
            }
        }
    }

}





