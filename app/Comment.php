<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['user_id', 'post_id', 'parent_id', 'comment'];

    public function post()
    {
        return $this->belongsTo('App\Post', 'post_id', 'id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Comment', 'parent_id', 'id');
    }

    public function children()
    {
        return $this->hasMany('App\Comment', 'parent_id', 'id');
    }
}
