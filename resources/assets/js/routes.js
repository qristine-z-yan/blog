import Home from './components/Home.vue';
import Category from './components/ShowCategoryComponent.vue';
import ShowUserPost from './components/ShowUserPostsComponent.vue';
import AddPost from './components/AddPostComponent.vue';
import MyPosts from './components/MyPostsComponent.vue';
import ShowPost from './components/ShowPostComponent.vue';
import EditPost from './components/EditPostComponent.vue';
import Example from './components/Example.vue';

export const routes = [
    { path: '/', component: Home, name: 'Home' },
    { path: '/category/:category_id', component: Category, name: 'Category' },
    { path: '/user/:user_id', component: ShowUserPost, name: 'ShowUserPost' },
    { path: '/add-post', component: AddPost, name: 'AddPost' },
    { path: '/my-posts', component: MyPosts, name: 'MyPosts' },
    { path: '/post/:post_id', component: ShowPost, name: 'ShowPost' },
    { path: '/edit-post/:post_id', component: EditPost, name: 'EditPost' },
    { path: '/vue/example', component: Example, name: 'Example' }
];