import Vue from 'vue'
import VueRouter from 'vue-router'
import axios from 'axios'
import { BreedingRhombusSpinner } from 'epic-spinners'

import { routes } from './routes'
Vue.use(VueRouter, axios);

window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
Vue.prototype.$http = window.axios;
Vue.prototype.$bus = new Vue({})

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

Vue.component('post-index', require('./components/PostComponent.vue'));
Vue.component('delete-modal', require('./components/ModalComponent.vue'));
Vue.component('add-comment', require('./components/AddCommentComponent.vue'));
Vue.component('view-comments', require('./components/ViewCommentsComponent.vue'));

const router = new VueRouter({
    routes
});
new Vue({
    el: '#app',
    router
});