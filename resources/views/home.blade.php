@extends('layouts.app')
@section('content')
    <section id="home">
                    <div class="theme-menu home">
                        <ul class="nav">
                @foreach($categories as $id => $name)
                    @if($loop->index >= 4)
                        <li class="nav-item dropdown theme">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">All</a>
                            <div class="dropdown-menu category-dropdawn">

                                @foreach($categories as $id => $name)

                                    @if($loop->index >= 4)

                                        <a class="nav-link opacity-9 dropdown-item margin-0"
                                           href="category/{{ $id }}">{{ $name }}</a>

                                    @endif

                                @endforeach
                            </div>
                        </li>
                        @break
                    @else
                        <li class="nav-item theme">
                            <a class="nav-link opacity-9" href="category/{{ $id }}">{{ $name }}</a>
                        </li>
                    @endif

                @endforeach

            </ul>
        </div>
        <div class="container">
            <div class="row">
                <div class="card col-4">
                    <div class="row">
                        @foreach($posts as $post)
                            @if($loop->index % 3 == 0)
                                <div class="col-12 margin-10-0">
                                    <div class="card-post">
                                        <div class="card-header">

                                            <img class="card-img-top" src="/img/posts/{{ $post['img'] }}"
                                                 alt="Card image">
                                        </div>
                                        <div class="card-body">
                                            <div>
                                                <h3 class="card-title d-inline-block">{{ $post['title'] }}
                                                    <a class="post-title-a"
                                                       href="{{ route('category.show',$post['category']['id']) }}">{{ $post['category']['name'] }}</a>
                                                </h3>

                                            </div>
                                            <div>
                                                <a href="{{ route('user.show',$post['user']['id']) }}"> {{ $post['user']['name'] }} {{ $post['user']['surname'] }}</a>
                                            </div>
                                            <div>
                                                <p class="card-text">{{ str_limit($post['body'], 100) }}</p>
                                            </div>
                                            <div>
                                                @if(Auth::check())

                                                    @if($post['author_id'] == auth()->id())
                                                        <a class="btn-red float-left" data-toggle="modal"
                                                           data-target="#deleteModal">Delete</a>
                                                        <a href="{{ route('post.edit', $post['id']) }}"
                                                           class="btn-red float-right">Edit</a>
                                                    @endif
                                                @endif
                                                <a href="{{ route('post.show', $post['id']) }}"
                                                   class="btn-red float-right">Read
                                                    more</a>

                                                <div class="modal fade" id="deleteModal">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">Delete post</h4>
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">&times;
                                                                </button>
                                                            </div>
                                                            <form action="{{ route('post.destroy', $post['id']) }}"
                                                                  method="POST" enctype="multipart/form-data">
                                                                {{ method_field('DELETE') }}
                                                                {{ csrf_field() }}

                                                                <div class="modal-body {{ $errors->has('add-category') ? ' has-error' : '' }}">
                                                                    Do you want really to delete your post?
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default"
                                                                            data-dismiss="modal">Close
                                                                    </button>
                                                                    <button type="submit" class="btn btn-red">Delete
                                                                    </button>
                                                                </div>
                                                            </form>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                        <div class="data d-flex justify-content-around">
                                            <div class="time">
                                                <i class="far fa-clock"></i>
                                                <span>{{ $post['created_at'] }}</span>
                                            </div>
                                            <div class="comments">
                                                <i class="fas fa-comment"></i>
                                                <span>{{ count($post->comments) }}</span> Comment
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <div class="follow d-flex justify-content-around">
                                                <i class="fab fa-instagram"></i>
                                                <i class="fab fa-facebook-f"></i>
                                                <i class="fab fa-twitter"></i>
                                                <i class="fab fa-google-plus-g"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
                <div class="card col-4">
                    <div class="row">
                        @foreach($posts as $post)
                            @if($loop->index % 3 == 1)
                                <div class="col-12 margin-10-0">
                                    <div class="card-post">
                                        <div class="card-header">

                                            <img class="card-img-top" src="/img/posts/{{ $post['img'] }}"
                                                 alt="Card image">
                                        </div>
                                        <div class="card-body">
                                            <div>
                                                <h3 class="card-title d-inline-block">{{ $post['title'] }}
                                                    <a class="post-title-a"
                                                       href="{{ route('category.show',$post['category']['id']) }}">{{ $post['category']['name'] }}</a>
                                                </h3>

                                            </div>
                                            <div>
                                                <a href="{{ route('user.show',$post['user']['id']) }}"> {{ $post['user']['name'] }} {{ $post['user']['surname'] }}</a>
                                            </div>
                                            <div>
                                                <p class="card-text">{{ str_limit($post['body'], 100) }}</p>
                                            </div>
                                            <div>
                                                @if(Auth::check())

                                                    @if($post['author_id'] == auth()->id())
                                                        <a class="btn-red float-left" data-toggle="modal"
                                                           data-target="#deleteModal">Delete</a>
                                                        <a href="{{ route('post.edit', $post['id']) }}"
                                                           class="btn-red float-right">Edit</a>
                                                    @endif
                                                @endif
                                                <a href="{{ route('post.show', $post['id']) }}"
                                                   class="btn-red float-right">Read
                                                    more</a>

                                                <div class="modal fade" id="deleteModal">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">Delete post</h4>
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">&times;
                                                                </button>
                                                            </div>
                                                            <form action="{{ route('post.destroy', $post['id']) }}"
                                                                  method="POST" enctype="multipart/form-data">
                                                                {{ method_field('DELETE') }}
                                                                {{ csrf_field() }}

                                                                <div class="modal-body {{ $errors->has('add-category') ? ' has-error' : '' }}">
                                                                    Do you want really to delete your post?
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default"
                                                                            data-dismiss="modal">Close
                                                                    </button>
                                                                    <button type="submit" class="btn btn-red">Delete
                                                                    </button>
                                                                </div>
                                                            </form>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="data d-flex justify-content-around">
                                            <div class="time">
                                                <i class="far fa-clock"></i>
                                                <span>{{ $post['created_at'] }}</span>
                                            </div>
                                            <div class="comments">
                                                <i class="fas fa-comment"></i>
                                                <span>{{ count($post->comments) }}</span> Comment
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <div class="follow d-flex justify-content-around">
                                                <i class="fab fa-instagram"></i>
                                                <i class="fab fa-facebook-f"></i>
                                                <i class="fab fa-twitter"></i>
                                                <i class="fab fa-google-plus-g"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
                <div class="card col-4">
                    <div class="row">
                        @foreach($posts as $post)
                            @if($loop->index % 3 == 2)
                                <div class="col-12 margin-10-0">
                                    <div class="card-post">
                                        <div class="card-header">

                                            <img class="card-img-top" src="/img/posts/{{ $post['img'] }}"
                                                 alt="Card image">
                                        </div>
                                        <div class="card-body">
                                            <div>
                                                <h3 class="card-title d-inline-block">{{ $post['title'] }}
                                                    <a class="post-title-a"
                                                       href="{{ route('category.show',$post['category']['id']) }}">{{ $post['category']['name'] }}</a>
                                                </h3>

                                            </div>
                                            <div>
                                                <a href="{{ route('user.show',$post['user']['id']) }}"> {{ $post['user']['name'] }} {{ $post['user']['surname'] }}</a>
                                            </div>
                                            <div>
                                                <p class="card-text">{{ str_limit($post['body'], 100) }}</p>
                                            </div>
                                            <div>
                                                @if(Auth::check())

                                                    @if($post['author_id'] == auth()->id())
                                                        <a class="btn-red float-left" data-toggle="modal"
                                                           data-target="#deleteModal">Delete</a>
                                                        <a href="{{ route('post.edit', $post['id']) }}"
                                                           class="btn-red float-right">Edit</a>
                                                    @endif
                                                @endif
                                                <a href="{{ route('post.show', $post['id']) }}"
                                                   class="btn-red float-right">Read
                                                    more</a>

                                                <div class="modal fade" id="deleteModal">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">Delete post</h4>
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">&times;
                                                                </button>
                                                            </div>
                                                            <form action="{{ route('post.destroy', $post['id']) }}"
                                                                  method="POST" enctype="multipart/form-data">
                                                                {{ method_field('DELETE') }}
                                                                {{ csrf_field() }}

                                                                <div class="modal-body {{ $errors->has('add-category') ? ' has-error' : '' }}">
                                                                    Do you want really to delete your post?
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default"
                                                                            data-dismiss="modal">Close
                                                                    </button>
                                                                    <button type="submit" class="btn btn-red">Delete
                                                                    </button>
                                                                </div>
                                                            </form>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                        <div class="data d-flex justify-content-around">
                                            <div class="time">
                                                <i class="far fa-clock"></i>
                                                <span>{{ $post['created_at'] }}</span>
                                            </div>
                                            <div class="comments">
                                                <i class="fas fa-comment"></i>
                                                <span>{{ count($post->comments) }}</span> Comment
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <div class="follow d-flex justify-content-around">
                                                <i class="fab fa-instagram"></i>
                                                <i class="fab fa-facebook-f"></i>
                                                <i class="fab fa-twitter"></i>
                                                <i class="fab fa-google-plus-g"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
