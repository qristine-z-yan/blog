<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name') }}</title>

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/my-app.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
    </head>
    <body>
        <div id="app">
            <nav class="navbar-default navbar-static-top">
                <div class="container">

                    <div class="collapse navbar-collapse" id="app-navbar-collapse">
                        <!-- Left Side Of Navbar -->
                            <div class="nav navbar-nav navbar-left">
                                <div class="logo">
                                    <router-link  to="/"><img src="/img/logo.png"></router-link>
                                </div>
                            </div>


                        <!-- Right Side Of Navbar -->
                            <ul class="nav navbar-nav navbar-right d-block">
                                <!-- Authentication Links -->
                                @if (Auth::guest())
                                    <li class="post-item"><a href="{{ route('login') }}">Login</a></li>
                                    <li class="post-item"><a href="{{ route('register') }}">Register</a></li>
                                @else
                                    @if(Auth::user()->is_admin == 1)
                                        <li class="post-item">
                                            <a href="{{ url('admin') }}">Administration</a>
                                        </li>
                                    @endif
                                            <li class="post-item"><router-link to="/add-post">Add post</router-link></li>
                                            <li class="post-item"><router-link to="/my-posts">My posts</router-link></li>
                                    <li class="main-dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" >
                                            {{ Auth::user()->name }}
                                        </a>

                                        <ul class="dropdown-menu" role="menu">
                                            <li class="post-item"><a href="{{ route('user.edit', Auth::user()->id) }}">Account Settings</a></li>
                                            <li>
                                                <a href="{{ route('logout') }}"
                                                    onclick="event.preventDefault();
                                                             document.getElementById('logout-form').submit();">
                                                    Logout
                                                </a>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                                      style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            </li>
                                        </ul>
                                    </li>
                                @endif
                            </ul>
                    </div>
                </div>
            </nav>
            {{--@yield('content')--}}
            <router-view></router-view>
        </div>
        <footer>

        </footer>

        <!-- Scripts -->

        <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js" integrity="sha384-SlE991lGASHoBfWbelyBPLsUlwY1GwNDJo3jSJO04KZ33K2bwfV9YBauFfnzvynJ" crossorigin="anonymous"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        @if(Auth::check())
            <script>
                localStorage.setItem('user-id', {{Auth::user()->id}});
            </script>
        @endif
        <script  src="{{ asset('js/facebook-login.js') }}"></script>
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('js/my-app.js') }}"></script>
        @yield('scripts')
    </body>
</html>
