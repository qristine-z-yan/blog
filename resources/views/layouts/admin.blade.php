<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="_token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'MyBlog') }}</title>

        <!-- Styles -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
        <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/material-dashboard.css') }}" rel="stylesheet">
        <link href="{{ asset('css/my-app.css') }}" rel="stylesheet">


    </head>
    <body>
    <div id="app">
        <nav class="navbar navbar-expand-lg navbar-dark indigo fixed-top scrolling-navbar">
            <div class="container">

                <div class="collapse navbar-collapse" id="app-navbar-collapse">

                    <!-- Left Side Of Navbar -->
                    <div class="nav navbar-nav mr-auto">
                        <div class="logo">
                            <a href="{{ url('/') }}"><img src="/img/logo.png"></a>
                        </div>
                    </div>

                    <!-- Right Side Of Navbar -->
                    <div class="nav navbar-nav ml-auto d-block">
                        <ul>
                        @if(Auth::user()->is_admin == 1)
                            <li class="post-item margin-10-0 admin-img">
                                <img src="../img/users/{{ Auth::user()->avatar }}" alt="Admin">
                            </li>
                        @endif
                        </ul>
                    </div>
                </div>
        </nav>
        <section id="administration">
            <div class="row">
                <div class="list-menu col-3">
                    <ul class="text-center">
                        <li class="list-item">
                            <a href="{{ url('admin') }}"><i class="far fa-bell"></i>Notifications</a>
                        </li>
                        <li class="list-item">
                            <a href="{{ url('admin/users') }}"><i class="far fa-user"></i>Users</a>
                        </li>
                        <li class="list-item">
                            <a href="{{ url('admin/posts') }}"><i class="far fa-bookmark"></i>Posts</a>
                        </li>
                        <li class="list-item">
                            <a href="{{ url('admin/categories') }}"><i class="far fa-cloud"></i>Categories</a>
                        </li>
                        <li class="list-item">
                            <a href="{{ url('admin/comments') }}"><i class="far fa-comments"></i>Comments</a>
                        </li>
                    </ul>
                </div>
                @yield('content')
            </div>
        </section>
        <footer>

        </footer>
    </div>

        <!-- Scripts -->
        <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js" integrity="sha384-SlE991lGASHoBfWbelyBPLsUlwY1GwNDJo3jSJO04KZ33K2bwfV9YBauFfnzvynJ" crossorigin="anonymous"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

        <script src="{{ asset('js/core/jquery.min.js') }}"></script>
        <script src="{{ asset('js/core/popper.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap-material-design.js') }}"></script>
        <script src="{{ asset('js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
        <!--  Charts Plugin, full documentation here: https://gionkunz.github.io/chartist-js/ -->
        <script src="{{ asset('js/plugins/chartist.min.js') }}"></script>
        <!-- Library for adding dinamically elements -->
        <script src="{{ asset('js/plugins/arrive.min.js') }}" type="text/javascript"></script>
        <!--  Notifications Plugin, full documentation here: http://bootstrap-notify.remabledesigns.com/    -->
        <script src="{{ asset('js/plugins/bootstrap-notify.js') }}"></script>
        <!-- Material Dashboard Core initialisations of plugins and Bootstrap Material Design Library -->
        <script src="{{ asset('js/material-dashboard.js?v=2.0.0') }}"></script>
        <script  src="{{ asset('facebook-login.js') }}"></script>
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('js/my-app.js') }}"></script>
    </body>
</html>
