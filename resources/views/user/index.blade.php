@extends('layouts.app')

@section('content')
    <section id="my-post">
        <div class="theme-menu post">
            <div class="container text-center padding-45">
                <div class="my d-inline">
                    My Post
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">

                @foreach($posts as $post)
                    <div class="card col-4">
                        <div class="card-post">
                            <div class="card-header">
                                <img class="card-img-top" src="/img/posts/{{ $post['img'] }}" alt="Card image"
                                     style="width:100%">
                            </div>
                            <div class="card-body">
                                <div>
                                    <h3 class="card-title d-inline-block">{{ $post['title'] }}</h3>
                                    <a class="post-title-a" href="{{ route('category.show',$post['category']['id']) }}">{{ $post['category']['name'] }}</a>
                                </div>
                                <div>
                                    <a href="{{ route('user.show',$post['author_id']) }}"> {{ $post['user']['name'] }} {{ $post['user']['surname'] }}</a>
                                </div>
                                <div>
                                    <p class="card-text">{{ $post['body'] }}</p>
                                </div>
                                <div>
                                    @if($post['author_id'] == Auth::user()->id)
                                        <a href="{{ route('post.show', $post['id']) }}" class="btn-red float-right">Read
                                            more</a>
                                        <a href="{{ route('post.edit', $post['id']) }}" class="btn-red float-right">Edit</a>
                                        <form action="{{ route('post.destroy', $post['id']) }}" method="POST"
                                              enctype="multipart/form-data">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <input type="submit" class="btn btn-red" value="Delete">
                                        </form>
                                    @else
                                        <a href="{{ route('post.show', $post['id']) }}" class="btn-red float-right">Read
                                            more</a>
                                    @endif
                                </div>

                            </div>
                            <div class="data d-flex justify-content-around">
                                <div class="time">
                                    <i class="far fa-clock"></i>
                                    <span>{{ $post['created_at'] }}</span>
                                </div>
                                <div class="comments">
                                    <i class="fas fa-comment"></i>
                                    <span></span> Comment
                                </div>
                                <div class="likes">
                                    <i class="fas fa-heart"></i>
                                    <span></span>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="follow d-flex justify-content-around">
                                    <i class="fab fa-instagram"></i>
                                    <i class="fab fa-facebook-f"></i>
                                    <i class="fab fa-twitter"></i>
                                    <i class="fab fa-google-plus-g"></i>
                                </div>
                            </div>
                        </div>
                    </div>

                @endforeach
            </div>
        </div>
    </section>

@endsection