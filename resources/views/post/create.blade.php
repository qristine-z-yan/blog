@extends('layouts.app')

@section('content')
    <section id="add-post">
        <div class="theme-menu post">
            <div class="container text-center padding-45">
                <div class="add d-inline">
                    Add Post
                </div>
            </div>
        </div>

        <div class="alert-div {{  session('status') }}"></div>

        <div class="container padding-30 margin-30">
            <form action="{{ route('post.store') }}" method="POST" enctype="multipart/form-data">

                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('post_title') ? ' has-error' : '' }}">
                    <label for="title">Title:</label>
                    <input type="text" class="form-control" id="title" name="title" value="{{ old('title') }}">


                    @if ($errors->has('title'))
                        <span class="help-block">
                             <strong>{{ $errors->first('title') }}</strong>
                        </span>
                    @endif

                </div>

                <div class="form-group{{ $errors->has('post_img') ? ' has-error' : '' }}">
                    <label for="post-img">Image:</label>
                    <input type="file" class="form-control" id="post-img" name="img" value="{{ old('img') }}">

                    @if ($errors->has('img'))
                        <span class="help-block">
                            <strong>{{ $errors->first('img') }}</strong>
                        </span>
                    @endif

                </div>

                <div class="form-group{{ $errors->has('posts') ? ' has-error' : '' }}">
                    <label for="body">Content:</label>
                    <textarea class="form-control" id="body" name="body">{{ old('body') }}</textarea>

                    @if ($errors->has('content'))
                        <span class="help-block">
                            <strong>{{ $errors->first('body') }}</strong>
                        </span>
                    @endif

                </div>
                <div class="form-group">

                    <label for="category">Select Category:</label>
                    {!! Form::select('category', $categories_data, null,['id' => 'category','class' => 'form-control']) !!}
                </div>

                <button class="btn-red" type="submit">Add Post</button>

            </form>
        </div>
    </section>
@endsection


