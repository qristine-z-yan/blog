@extends('layouts.app')



@section('content')
    @if(session('status') == 'comment-added')
        <script>
            toastr.success("Comment successfully added!");
        </script>
    @endif

    {{--<div class="alert-div {{ session('status') }}"></div>--}}
    <section>
        <div class="theme-menu">
            <div class="container text-center padding-45">
                <div class="my d-inline ">
                    {{ $post['category']['name'] }}
                </div>
            </div>
        </div>
        <div class="container">
            <div class="post-author margin-10-0">
                <img src="/img/users/{{ $post['user']['avatar'] }}" alt="User avatar">
                <a href="{{ route('user.show',$post['user']['id']) }}">{{ $post['user']['name'] }} {{ $post['user']['surname'] }}</a>
            </div>
            <div class="post-title">
                <h2 class="d-inline">{{ $post['title'] }}</h2>
                <a class="post-title-a" href="{{ $post['category']['id'] }}"></a>
            </div>
            <div class="post-time">
                {{ $post['created_at'] }}
            </div>
            <div class="row">
                <div class="post-img col-4">
                    <img src="/img/posts/{{ $post['img'] }}">
                </div>
                <div class="col-8">
                    <p>{{ $post['body'] }}</p>
                </div>
            </div>
            <div id="comment">
                <span><b>Comments</b></span>
                <div class="row">
                    @if(Auth::check())
                        <add-comment :parent-id="parentId"></add-comment>
                        <view-comments  :comments="comments"></view-comments>
                    @else
                        <div class="alert alert-info">
                            <strong>Attention!</strong> For add  or read comments, please Log In or Sign Up.
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $('.theme-menu').css({ "background":"url('../img/categories/{{ $post->category->img }}') no-repeat",
        "background-size":"cover"
        });
        localStorage.setItem('post-id',{{$post->id}});

    </script>
@endsection