@extends('layouts.app')

@section('content')

    <section id="edit-post">
        <div class="theme-menu post">
            <div class="container text-center padding-45">
                <div class="add d-inline">
                    Edit Post
                </div>
            </div>
        </div>
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="container padding-30 margin-30">
            <form action="{{ route('post.update',$post['id']) }}" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_method" value="put">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    <label for="title">Title:</label>
                    <input type="text" class="form-control" id="title" name="title" value="{{ $post['title'] }}">

                </div>

                <div class="form-group{{ $errors->has('img') ? ' has-error' : '' }} edit-img">
                    <label for="post-img" class="d-block">Image:</label>
                    <img src="/img/posts/{{ $post['img'] }}" >
                    <input type="file" class="form-control" id="post-img" name="img" value="{{ old('img') }}">

                </div>

                <div class="form-group">
                    <label for="category">Select Category:</label>
                    {!! Form::select('categories', $categories_data, $post['category_id'],['id' => 'category','class' => 'form-control']) !!}
                </div>

                <div class="form-group{{ $errors->has('post') ? ' has-error' : '' }}">

                    <label for="body">Content:</label>
                    <textarea class="form-control" id="body" name="body">{{ $post['body'] }}</textarea>

                </div>

                <button class="btn-red" type="submit">Edit Post</button>

            </form>
        </div>
    </section>
@endsection