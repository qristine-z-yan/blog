@extends('layouts.admin')

@section('content')
    <div class="col-9">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header card-header-info">
                        <h4 class="card-title mt-0">Users Table</h4>
                        <p class="card-category"> Here is a subtitle for this table</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                <th>
                                    ID
                                </th>
                                <th>
                                    User
                                </th>
                                <th>
                                    Post ID
                                </th>
                                <th>
                                    Parent ID
                                </th>
                                <th>
                                    Comment
                                </th>
                                <th>
                                    Data
                                </th>
                                <th>
                                    Childs count
                                </th>
                                <th>
                                    Delete
                                </th>
                                </thead>
                                <tbody>
                                @foreach($comments as $comment)
                                    <tr @if(Auth::user()->id == $comment->author_id)class="table-active" @endif>
                                        <td>{{ $comment->id }}</td>
                                        <td>{{ $comment->users->name}}  {{ $comment->users->surname}}</td>
                                        <td>{{ $comment->post_id }}</td>
                                        <td>{{ $comment->parent_id }}</td>
                                        <td>{{ $comment->comment }}</td>
                                        <td>{{ $comment->updated_id }}</td>
                                        <td>{{ Count($comment->child)}}</td>
                                        <td>
                                            <button type="button" class="btn btn-danger waves-effect waves-light btn-comment" data-toggle="modal" data-target="#delete" data-id="{{ $comment->id }}"><i class="fas fa-trash"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                                <div class="modal fade" id="delete">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Delete post</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                Do you want really to delete this comment?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-red delete-comment" data-id="">Delete</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
@endsection