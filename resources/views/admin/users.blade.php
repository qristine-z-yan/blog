@extends('layouts.admin')

@section('content')
    <div class="col-9">
        <div class="row">
            <div class="col-12">
                <div class="card">
                        <div class="card-header card-header-primary" data-background-color="purple">
                            <h4 class="card-title mt-0">Users Table</h4>
                            <p class="card-category"> Here is a subtitle for this table</p>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class=" text-primary">
                                    <th>
                                        ID
                                    </th>
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        Surname
                                    </th>
                                    <th>
                                        Email
                                    </th>
                                    <th>
                                        Phone
                                    </th>
                                    <th>
                                        Avatar
                                    </th>
                                    <th>
                                        Posts count
                                    </th>
                                    <th>
                                        Admin
                                    </th>
                                    </thead>
                                    <tbody>
                                        @foreach($users as $user)
                                            <tr @if(Auth::user()->id == $user->id)class="table-active" @endif>
                                                <td>{{ $user->id }}</td>
                                                <td>{{ $user->name }}</td>
                                                <td>{{ $user->surname }}</td>
                                                <td>{{ $user->email }}</td>
                                                <td>{{ $user->phone }}</td>
                                                <td><img src="../img/users/{{ $user->avatar }}" alt="User avatar" class="user-img"></td>
                                                <td><a href="{{ route('user.show',$user->id) }}">{{ count($user->posts) }}</a></td>
                                                <td><input type="checkbox" id="switch-{{$user->id}}"  name="set-name" class="switch-input" @if($user->is_admin == 1) checked @endif data-id="{{$user->id}}">
                                                    <label for="switch-{{$user->id}}" class="switch-label" ></label>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                </div>
            </div>
                </div>
            </div>
        </div>
    </div>
@endsection