@extends('layouts.admin')

@section('content')
    <div class="col-9">
        <p class="welcome text-center margin-30">Welcome {{ Auth::user()->name}} {{ Auth::user()->surname}}</p>
        <div class="row text-center">
            <div class="col-3">
                <div class="blog-progress">
                    <div class="icon">
                        <i class="fas fa-users"></i>
                    </div>
                    <div class="progress-item">
                        <div class="progress-count">{{ count($users) }}</div>
                        <div class="progress-name">users</div>
                        <div class="text">Count of users registered in Amelie </div>
                    </div>
                    <div class="more-info d-inline-block">
                        <div class="btn btn-default ">
                            <a href="{{ url('admin/users') }}">More Info</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="blog-progress">
                    <div class="icon">
                        <i class="fas fa-bookmark"></i>
                    </div>
                    <div class="progress-item">
                        <div class="progress-count">{{ count($posts) }}</div>
                        <div class="progress-name">posts</div>
                        <div class="text">Count of posts showed in Amelie </div>
                    </div>
                    <div class="more-info d-inline-block">
                        <div class="btn btn-default ">
                            <a href="{{ url('admin/posts') }}">More Info</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="blog-progress">
                    <div class="icon">
                        <i class="fas fa-certificate"></i>
                    </div>
                    <div class="progress-item">
                        <div class="progress-count">{{ count($categories) }}</div>
                        <div class="progress-name">categories</div>
                        <div class="text">Count of categories of posts in Amelie </div>
                    </div>
                    <div class="more-info d-inline-block">
                        <div class="btn btn-default ">
                            <a href="{{ url('admin/categories') }}">More Info</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="blog-progress">
                    <div class="icon">
                        <i class="fas fa-comment"></i>
                    </div>
                    <div class="progress-item">
                        <div class="progress-count">{{ count($comments) }}</div>
                        <div class="progress-name">comments</div>
                        <div class="text">Count of comments in posts Amelie </div>
                    </div>
                    <div class="more-info d-inline-block">
                        <div class="btn btn-default ">
                            <a href="{{ url('admin/comments') }}">More Info</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection