 @extends('layouts.admin')

@section('content')
    {{--@if (session()->has('type'))--}}
         {{--<div class="alert alert-info">{{ session('type') }}</div>--}}
        {{--<div class="alert alert-info">{{ session()->get('message') }}</div>--}}
    {{--@endif--}}
    <div class="col-9">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header card-header-info">
                        <h4 class="card-title mt-0">Categories Table</h4>
                        <p class="card-category"> Here is a subtitle for this table</p>
                    </div>
                    <div class="card-body">
                        <div class="add-category float-right margin-10-0">
                            <button class="btn btn-success" data-toggle="modal" data-target="#add">Add category</button>
                        </div>
                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-info">
                                <th>
                                    ID
                                </th>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Image
                                </th>
                                <th>
                                    Date
                                </th>
                                <th>
                                    Delete
                                </th>
                                </thead>
                                <tbody id="categories">
                                    @foreach($categories as $category)
                                        <tr class="category-{{ $category->id }}">
                                            <td>{{ $category->id }}</td>
                                            <td>{{ $category->name }}</td>
                                            <td><img class="category-img" src="../img/categories/{{ $category->img }}" alt=""></td>
                                            <td>{{ $category->updated_at }}</td>
                                            <td>
                                                <button type="button" class="btn btn-danger waves-effect waves-light btn-category" data-toggle="modal" data-target="#delete" data-id="{{ $category->id }}"><i class="fas fa-trash"></i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="modal fade" id="delete">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Delete category</h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            Do you want really to delete this category?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-red delete-category" data-id="">Delete</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="add">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form  action="{{ route('category.store') }}" method="POST" enctype="multipart/form-data">
                                            <div class="modal-header card-header-success">
                                                <h4 class="modal-title">Add Category</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            {{ csrf_field() }}
                                            <div class="modal-body">
                                                <div class="{{ $errors->has('category-name') ? ' has-error' : '' }}">
                                                    <label for="category-name">Name Category:</label>
                                                    <input type="text" class="form-control text-capitalize" id="category-name" name="category-name" value="{{ old('name') }}">

                                                    @if ($errors->has('category-name'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('category-name') }}</strong>
                                                        </span>
                                                    @endif

                                                </div>

                                                <div class="{{ $errors->has('category-img') ? ' has-error' : '' }}">
                                                    <label for="category-img">Image:</label>
                                                    <input type="file" class="form-control" id="category-img" name="category-img">

                                                    @if ($errors->has('category-img'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('category-img') }}</strong>
                                                        </span>
                                                    @endif

                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-red" >Add</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')


@endsection
