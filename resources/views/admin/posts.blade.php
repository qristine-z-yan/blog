@extends('layouts.admin')

@section('content')
    <div class="col-9">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header card-header-info">
                        <h4 class="card-title mt-0">Posts Table</h4>
                        <p class="card-category"> Here is a subtitle for this table</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                    <th>
                                        ID
                                    </th>
                                    <th>
                                        Author ID
                                    </th>
                                    <th>
                                        Category
                                    </th>
                                    <th>
                                        Title
                                    </th>
                                    <th>
                                        Image
                                    </th>
                                    <th>
                                        Body
                                    </th>
                                    <th>
                                        Date
                                    </th>
                                    <th>
                                        Comment Count
                                    </th>
                                    <th>
                                        Delete
                                    </th>
                                </thead>
                                <tbody>
                                @foreach($posts as $post)
                                    <tr @if(Auth::user()->id == $post->author_id)class="table-active" @endif>
                                        <td>{{ $post->id }}</td>
                                        <td>{{ $post->author_id }}</td>
                                        <td>{{ $post->category->name }}</td>
                                        <td>{{ str_limit($post->title, 20) }}</td>
                                        <td><img src="../img/posts/{{ $post->img }}" alt="Post img" class="admin-post-img"></td>
                                        <td>{{ str_limit($post->body, 20) }}</td>
                                        <td>{{ $post->updated_at }}</td>
                                        <td><a href="#">{{ count($post->comment) }}</a></td>
                                        <td>
                                            <button type="button" class="btn btn-danger waves-effect waves-light btn-post" data-toggle="modal" data-target="#delete" data-id="{{ $post->id }}"><i class="fas fa-trash"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                                <div class="modal fade" id="delete">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Delete post</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                Do you want really to delete this post?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-red delete-post" data-id="">Delete</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection