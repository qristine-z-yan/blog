<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/get-posts', 'HomeController@getPosts');
Route::get('confirm/{token}/{email}','UserController@index');
Route::post('user/change-password','UserController@changePassword');
Route::resource('user', 'UserController');


Route::get('redirect', 'SocialAuthFacebookController@redirect');
Route::get('callback', 'SocialAuthFacebookController@callback');

Route::get('/get-url','PostController@getURL');
Route::post('/post/{id}','PostController@updatePost');
Route::resource('post', 'PostController');
Route::resource('category', 'CategoryController');

Route::get('all-comments/{post}', 'CommentController@showComments');
Route::resource('comments', 'CommentController');

Route::prefix('admin')->group(function () {
    Route::get('/', 'AdminController@index');
    Route::get('notification', 'AdminController@notification');
    Route::get('users', 'AdminController@users');
    Route::get('update-admin', 'AdminController@updateAdmin');
    Route::get('posts', 'AdminController@posts');
    Route::get('categories', 'AdminController@categories');
    Route::get('comments', 'AdminController@comments');
});
