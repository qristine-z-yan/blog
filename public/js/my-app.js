$.ajaxSetup({
    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});

$( document ).ready(function(){
    $('.col-4').height() == $('.card-post').height();
})

if ($('.alert-div').hasClass('category')) {
    toastr.success("Category successfully added!");
    $('.alert-div').removeClass('category')
}

if ($('.alert-div').hasClass('post-added')) {
    toastr.success("Post successfully added!");
    $('.alert-div').removeClass('post-added')
}

if ($('.alert-div').hasClass('post-updated')) {
    toastr.success("Post successfully updated!");
    $('.alert-div').removeClass('post-updated')
}

if ($('.alert-div').hasClass('post-deleted')) {
    toastr.success("Post successfully deleted!");
    $('.alert-div').removeClass('post-deleted')
}

if ($('.alert-div').hasClass('not-loggined')) {
    toastr.info("For add comment please Sign Up or Log In !");
    $('.alert-div').removeClass('comment-added')
}

if ($('.alert-div').hasClass('comment-added')) {
    toastr.success("Comment successfully added!");
    $('.alert-div').removeClass('comment-added')
}

if ($('.alert-div').hasClass('comment-deleted')) {
    toastr.info("Comment successfully deleted!");
    $('.alert-div').removeClass('comment-deleted')
}

if ($('.alert-div').hasClass('comment-error')) {
    toastr.error("Edited comment cannot be same as your comment. Please write a different comment or click on Close.");
    $('.alert-div').removeClass('password-error');
}

if ($('.alert-div').hasClass('comment-updated')) {
    toastr.success("Your comment updated.");
    $('.alert-div').removeClass('comment-updated');
}

if ($('.alert-div').hasClass('password-changed')) {
    toastr.success("Password changed successfully !");
    $('.alert-div').removeClass('password-changed');
}

if ($('.alert-div').hasClass('password-error')) {
    toastr.error("New Password cannot be same as your current password. Please choose a different password.");
    $('.alert-div').removeClass('password-error');
}

if ($('.alert-div').hasClass('password-matches')) {
    toastr.error("Your current password does not matches with the password you provided. Please try again.");
    $('.alert-div').removeClass('password-matches');
}

$('body').on('change', '.switch-input', function () {
    if($(this).is(':checked')){
        var val = '1';
    } else {
        val = '0';
    }
    var id = $(this).attr('data-id');
         $.ajax({
             url: 'admin/update-admin',
             type: 'get',
             data: {id:id,value:val},
             success: function (res) {
                    toastr.success(res['msg']);
             },
             error: function(res) {
                     toastr.error(res['msg'])
             }
         })
})

$('body').on('click', '.btn-post', function () {
    id = $(this).attr('data-id');
    $('.delete-post').attr('data-id', id)
})

$('body').on('click', '.delete-post', function () {
    if($('.delete-post').attr('data-id') != '') {
        id = $('.delete-post').attr('data-id');
        $('.modal').removeClass('show in');
        $('.modal-backdrop').removeClass('show');
        $.ajax({
            url: '/post/' + id,
            type: "DELETE",
            data: {id:id},
            success: function(res){
                    toastr.success(res['msg']);
                    location.reload();
            },
            error: function(res){
                    toastr.error(res['msg']);
            }
        })
    }
})

$('body').on('click', '.btn-category', function () {
    id = $(this).attr('data-id');
    $('.delete-category').attr('data-id', id)
})

$('body').on('click', '.delete-category', function () {
    if($('.delete-category').attr('data-id') != '') {
        id = $('.delete-category').attr('data-id');
        $('.modal').removeClass('show in');
        $('.modal-backdrop').removeClass('show');
        console.log('.category-' + id)
        $.ajax({
            url: '/category/' + id,
            type: "DELETE",
            data: {id:id},
            success: function(res){
                toastr.success(res['msg']);
                $('#categories').find('.category-' + id).remove();
            },
            error: function(res){
                toastr.error(res['msg']);
            }
        })
    }
})

$('body').on('click', '.btn-comment', function () {
    id = $(this).attr('data-id');
    $('.delete-comment').attr('data-id', id)
})

$('body').on('click', '.delete-comment', function () {
    if($('.delete-comment').attr('data-id') != '') {
        id = $('.delete-comment').attr('data-id');
        $('.modal').removeClass('show in');
        $('.modal-backdrop').removeClass('show');
        $.ajax({
            url: '/comments/' + id,
            method: "DELETE",
            data: {id:id},
            success: function(res){
                    // toastr.success(res['msg']);
                    // location.reload();
            },
            error: function(res){
                    toastr.error('Not found');
            }
        })
    }
})